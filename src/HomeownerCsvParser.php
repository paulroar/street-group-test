<?php

class HomeownerCsvParser 
{
	public $filename;

	const TITLES = [
		'mr',
		'mrs',
		'mister',
		'ms',
		'dr',
		'prof'
	];

	const PUNCTUATION_CONJUNCTIONS = [
		'&',
		','
	];

	const WORD_CONJUNCTIONS = [
		'and'
	];

	const FALLBACK_TITLE = 'N/A';

	public function __construct(Array $args)
	{
		$this->filename = $args['filename'];
	}


	public function parseFile() : Array 
	{
		if(! file_exists($this->filename)){
			throw new Exception('File does not exist');
		}

		$fp = fopen($this->filename,'r');

		$results = [];
		$lineNumber = 0;
		while($unparsed = fgetcsv($fp)){
			$lineNumber++;
			if($lineNumber == 1){
				continue;
			}
			$individuals = $this->parseLine($unparsed[0]);
			foreach ($individuals as $individual) {
				$results[] = $individual;
			}
		}

		return $results;
	}

	private function conjunctions() : Array
	{
		return array_merge(HomeownerCsvParser::WORD_CONJUNCTIONS, HomeownerCsvParser::PUNCTUATION_CONJUNCTIONS);
	}

	private function parseName(String $unparsed) : Array
	{
		$words = preg_split('/\s+/', $unparsed, -1, PREG_SPLIT_NO_EMPTY);

		$title = null;
		$first_name = null;
		$last_name = null;
		$initial = null;

		if(! $words){
			return[];
		}


		if(in_array(strtolower($words[0]), HomeownerCsvParser::TITLES)){
			$title = array_shift($words);
		}
		while(count($words) > 1){
			$first_name .= ' ' . array_shift($words);
		}
		if($first_name){
			$first_name = trim($first_name);
		}
		if($words){
			$last_name = $words[0];
		}
		if(strlen($first_name) == 1){
			$initial = $first_name;
			$first_name = null;
		}
		if(! $title){
			$title = HomeownerCsvParser::FALLBACK_TITLE;
		}
		return [ 'title' => $title, 'first_name' => $first_name, 'initial' => $initial, 'last_name' => $last_name ];
	}

	private function parseLine(String $unparsed) : Array
	{
		$individuals = [];

		$unparsed = str_replace('.', '', $unparsed);

		// add spaces before punctuation conjunctions, there may not be one before a comma, say
		foreach(HomeownerCsvParser::PUNCTUATION_CONJUNCTIONS as $conjunction){
			$unparsed = str_ireplace($conjunction, ' ' . $conjunction,$unparsed );
		}

		// make all punctuation conjunctions the same to aid in splitting
		foreach($this->conjunctions() as $conjunction){
			$unparsed = str_ireplace($conjunction, '&',$unparsed);
		}

		$unparsedNames = explode('&', $unparsed);

		$individuals = [];
		foreach($unparsedNames as $unparsed){
			$parsed = $this->parseName($unparsed);
			if($parsed){
				$individuals[] = $parsed;
			}
		}


		$individualWithLastName;
		foreach($individuals as $individual){
			if($individual['last_name']){
				$individualWithLastName = $individual;
			}
		}

		foreach($individuals as &$individual){
			if(! $individual['last_name']){
				$individual['last_name'] = $individualWithLastName['last_name'];
				$individual['first_name'] = $individualWithLastName['first_name'];
			}
		}


		return $individuals;
	}
}