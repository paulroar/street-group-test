# Things to note

- In a scheme of 'first name', 'initial' , 'last name', 'initial' usually refers to a middle initial, rather than the initial of the first name as spec'ed here
- If someone actually has the name of 'and' or 'And', then this is going to mess up the parser. More work is needed to handle the case of a name being the same as one of the conjunctions
- Don't take much mind of the times of commits, I've dipped in and out this afternoon.


