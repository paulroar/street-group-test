<?php
include __DIR__ . '/../src/HomeownerCsvParser.php';

use PHPUnit\Framework\TestCase;

class HomeownerCsvParserTest extends TestCase
{
	public function testConstructor() : void
	{
		$parser = new HomeownerCsvParser(['filename' => __DIR__ . '/tests.csv']);
		$this->assertEquals($parser->filename,__DIR__ . '/tests.csv','Filename set in constructor');
	}

	public function testFilenameProvidedToParseExists()
	{
		$parser = new HomeownerCsvParser(['filename' => __DIR__ . '/does-not-exist.csv']);

		$this->expectException(Exception::class);

		$parser->parseFile();
	}

	public function testParseFile()
	{
		$parser = new HomeownerCsvParser(['filename' => __DIR__ . '/tests.csv']);

		$expected = [
			[
				'title' => 'Mr',
				'first_name' => 'John',
				'initial' => null,
				'last_name' => 'Smith'
			],
			[
				'title' => 'Mrs',
				'first_name' => 'Jane',
				'initial' => null,
				'last_name' => 'Smith'
			],
			[
				'title' => 'Mister',
				'first_name' => 'John',
				'initial' => null,
				'last_name' => 'Doe'
			],
			[
				'title' => 'Mr',
				'first_name' => null,
				'initial' => null,
				'last_name' => 'Smith'
			],
			[
				'title' => 'Mrs',
				'first_name' => null,
				'initial' => null,
				'last_name' => 'Smith'
			],
			[
				'title' => 'Mr',
				'first_name' => null,
				'initial' => 'M',
				'last_name' => 'Mackie'
			],
			[
				'title' => 'Mr',
				'first_name' => 'Tom',
				'initial' => null,
				'last_name' => 'Staff'
			],
			[
				'title' => 'Mr',
				'first_name' => 'John',
				'initial' => null,
				'last_name' => 'Doe'
			],
			[
				'title' => 'Dr',
				'first_name' => null,
				'initial' => 'P',
				'last_name' => 'Gunn'
			],
			[
				'title' => 'Dr',
				'first_name' => 'Joe',
				'initial' => null,
				'last_name' => 'Bloggs'
			],
			[
				'title' => 'Mrs',
				'first_name' => 'Joe',
				'initial' => null,
				'last_name' => 'Bloggs'
			],
			[
				'title' => 'Ms',
				'first_name' => 'Claire',
				'initial' => null,
				'last_name' => 'Robbo'
			],
			[
				'title' => 'Prof',
				'first_name' => 'Alex',
				'initial' => null,
				'last_name' => 'Brogan'
			],
			[
				'title' => 'Mrs',
				'first_name' => 'Faye',
				'initial' => null,
				'last_name' => 'Hughes-Eastwood'
			],
			[
				'title' => 'Mr',
				'first_name' => null,
				'initial' => 'F',
				'last_name' => 'Fredrickson'
			],
			[
				'title' => 'Mr',
				'first_name' => null,
				'initial' => null,
				'last_name' => 'Hughes'
			],
			[
				'title' => 'Mr',
				'first_name' => null,
				'initial' => null,
				'last_name' => 'Jones'
			],
			[
				'title' => 'N/A',
				'first_name' => 'Dave',
				'initial' => null,
				'last_name' => 'Preston'
			],
			[
				'title' => 'N/A',
				'first_name' => null,
				'initial' => 'F',
				'last_name' => 'Jones'
			]
		];

		$this->assertEquals($parser->parseFile(),$expected, 'Parser produces expected array');
	}
}